`timescale 1ns / 1ps

module MIPS32_TestBench();

	reg  clock;
  reg  reset;
  reg  [4:0] Interrupts;            // 5 general-purpose hardware interrupts
  reg  NMI;                         // Non-maskable interrupt

  reg  [31:0] DataMem_In;
  reg  DataMem_Ack;
  wire DataMem_Read; 
  wire [3:0]  DataMem_Write;        // 4-bit Write; one for each byte in word.
  wire [29:0] DataMem_Address;      // Addresses are words; not bytes.
  wire [31:0] DataMem_Out;

  reg  [31:0] InstMem_In;
  wire [29:0] InstMem_Address;      // Addresses are words; not bytes.
  reg  InstMem_Ack;
  wire InstMem_Read;
  wire [7:0] IP;

  initial clock = 0;
	always #1 clock = ~clock;

	Processor mips32(
		    .clock(),
		    .reset(),
		    .Interrupts(),
		    .NMI(),

		    .DataMem_In(),
		    .DataMem_Ack(),
		    .DataMem_Read(),
		    .DataMem_Write(),
		    .DataMem_Address(),
		    .DataMem_Out(),

		    .InstMem_In(),
		    .InstMem_Address(),
		    .InstMem_Ack(),
		    .InstMem_Read(),
		    .IP()
		);

	initial begin
		InstMem_In = 0;
	end

endmodule