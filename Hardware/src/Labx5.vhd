library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity Labx5 is
generic(
   DEBOUNCE    	: integer := 10000000;  -- filtro ruido de trepidaçao, 8000000*20ns = 0,16s
   CICLES_SECOND 	: integer := 50000000  -- Frequencia do clock em Hz
   );
port(
		CLOCK_50MHz		: in std_logic;
		KEY				: in std_logic;
		DISP0_D        : out std_logic_vector (6 downto 0);

		-- Sensor de Temperatura
		I2C_SCL			: OUT STD_LOGIC;
		I2C_SDA			: INOUT STD_LOGIC
	);
end Labx5;

architecture behavior of Labx5 is

constant	ADDRESS_SENSOR : std_logic_vector (6 downto 0) := "1001000";
constant RD : std_logic := '1';
constant WR : std_logic := '0';
constant TEMP_REG : std_logic_vector(3 downto 0) := "0000";
constant CONF_REG : std_logic_vector(3 downto 0) := "0001";
constant TLOW_REG : std_logic_vector(3 downto 0) := "0010";
constant THIGH_REG : std_logic_vector(3 downto 0) := "0011";

signal clk_50mhz           : std_logic;
signal rst                 : std_logic;
signal start_write	      : std_logic_vector (0 downto 0) := (others => '0');
signal i2c_read 	         : std_logic_vector (11 downto 0);
signal i2c_busy	         : std_logic_vector (0 downto 0);
signal i2c_address         : std_logic_vector (7 downto 0);
signal i2c_write	         : std_logic_vector (15 downto 0);
signal i2c_count	         : integer range (CICLES_SECOND -1) downto 0 := 0;
signal uni_temp, dec_temp  : integer range 0 to 9;

-- Declaraçao Sensor de Temperatura
COMPONENT i2c_temperatura
	PORT
	(
		clk_i				:	 IN STD_LOGIC;
		start_write_i		:	 IN STD_LOGIC;
		i2c_address_i		:	 IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		i2c_write_i		:	 IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		i2c_sda_io		:	 INOUT STD_LOGIC;
		i2c_scl_o		:	 OUT STD_LOGIC;
		i2c_busy_o		:	 OUT STD_LOGIC;
		i2c_read_o		:	 OUT STD_LOGIC_VECTOR(11 DOWNTO 0)
	);
END COMPONENT;

component pll
	PORT
	(
		inclk0		: IN STD_LOGIC  := '0';
		c0		      : OUT STD_LOGIC
	);
end component;

-- Declaraçao do decodificador para display de 7 segmentos
component display_7seg is
	port(
		data_i		:	in std_logic_vector(3 downto 0);
		disp_7seg_o	:	out std_logic_vector(6 downto 0)

	);
end component;

-- Declaração sincronizador de reset
COMPONENT reset_sync
   PORT (
		i_clk	               :	 IN STD_LOGIC;
		i_external_reset_n	:	 IN STD_LOGIC;
		o_reset_n      		:	 OUT STD_LOGIC;
		o_reset		         :	 OUT STD_LOGIC
   );
END COMPONENT;


begin -- behavior

   pll_inst : pll PORT MAP (
		inclk0	=> CLOCK_50MHz,
      c0	      => clk_50mhz
	);

   --rst <= KEY(2);
	reset_synch_50mhz_inst : reset_sync
	PORT MAP (
		i_clk                => clk_50mhz,
		i_external_reset_n   => KEY(2),
		o_reset		         => open,
		o_reset_n            => rst
	);

	-- Instanciacao Sensor Temperatura
	inst_sensor: i2c_temperatura
	PORT MAP
	(
		clk_i				=> clk_50mhz,
		start_write_i	=> start_write(0),
		i2c_address_i	=> i2c_address,
		i2c_write_i		=> i2c_write,
		i2c_sda_io		=> i2c_SDA,
		i2c_scl_o		=> i2c_SCL,
		i2c_busy_o		=> i2c_busy(0),
		i2c_read_o		=> i2c_read
	);


	-- Processo para controle do sensor de temperatura
	process (clk_50mhz, rst)
	variable flag_init : std_logic := '0';	--	Flag para indicar se o sensor ja foi inicializado
	variable count_init: integer range 0 to 3 := 0;	-- Conta em qual estado a inicializaçao esta
	begin
	if rst = '1' then
		i2c_count <= 0;
		start_write(0) <= '0';
		i2c_write <= x"0000";
		i2c_address <= x"00";
		flag_init := '0';
		count_init := 0;
	elsif rising_edge (clk_50mhz) then
		if flag_init = '0' then
			if i2c_busy(0) = '0' then
				case count_init is
					-- Registrador de Controle
					when 0 =>	-- 9bits de resolucao, 6 fault, polaridade negativa, modo comparador, sem shutdown
						start_write(0) <= '1';
						i2c_write <= x"18" & x"0" & CONF_REG;
						i2c_address <= ADDRESS_SENSOR & WR;
						count_init := count_init + 1;
					-- Minimo alarme temperatura
					when 1 =>
						start_write(0) <= '1';
						i2c_write <= x"0F0" & TLOW_REG;	--	15 graus
						i2c_address <= x"90";
						count_init := count_init + 1;
					when 2 =>
					-- Maximo alarme temperatura
						start_write(0) <= '1';
						i2c_write <= x"320" & THIGH_REG;	-- 50 graus
						i2c_address <= ADDRESS_SENSOR & WR;
						count_init := count_init + 1;
					-- Seleçao do registrador para ler temperatura
					when 3 =>
						start_write(0) <= '1';
						i2c_write <= x"000" & TEMP_REG;
						i2c_address <= ADDRESS_SENSOR & WR;
				end case;
			else
				start_write(0) <= '0';
				if count_init = 3 then
					flag_init := '1';
					count_init := 0;
				end if;
			end if;
		else
			i2c_write <= x"0000";
			i2c_address <= x"91";
			if i2c_count = 0 then
				i2c_count <= CICLES_SECOND - 1;
				if (i2c_busy(0) = '0') then
					start_write(0) <= '1';
				end if;
			else
				i2c_count <= i2c_count -1;
				if i2c_busy(0) = '1' then
					start_write(0) <= '0';
				else
						DISP0_D <= (i2c_read(10 downto 4))      -- Escrita da dezena da temperatura
				end if;
			end if;
		end if;
	end if;
	end process;





end behavior;
