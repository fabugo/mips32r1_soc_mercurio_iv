module Timer (
  input clock,
  input rst,
  input prescaler,

  output  interrupt,
  );

  reg sum;




  always @ ( posedge clock ) begin
      if (sum <= prescaler ) sum = sum + 1;
      else sum = 0;

      case (prescaler)
        2: interrupt  = sum[2];
        4: interrupt  = sum[3];
        8: interrupt  = sum[4];
        16: interrupt  = sum[5];
        default: ;
      endcase
  end
endmodule // Timer
