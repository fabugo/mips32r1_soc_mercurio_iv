module Celsius (
  input  clock_50MHz,
  input   rst,
  input Read,

  output Ack,
  output [6:0] temp_out,
  output  I2C_SCL,
  inout I2C_SDA
  );

// ==========================================================

//                        WIRES/REG

// =========================================================

reg [6:0 ]d_temp;


// ==========================================================

//                        MODULES

// =========================================================

  Labx5 i2c_sensor (
      .CLOCK_50MHz    (clock_50MHz),
      .KEY            (rst),
      .DISP0_D        (d_temp),
      .I2C_SCL        (I2C_SCL),
      .I2C_SDA        (I2C_SDA)
  );

  // ==========================================================

  //                        SOURCE

  // =========================================================


  always @(posedge clock) begin
      Ack <= (rst) ? 1'b0 : (Read);
      temp_out <= d_temp;
  end


endmodule // temp
